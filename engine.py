from itertools import product
from enum import Enum
import itertools


"""A State in the wireworld"""
class State(Enum):
    conductor = '#888888'       # grey
    head      = '#c81e0f'       # red
    tail      = '#b43092'       # magenta

    """Return the state corresponding to a character.
    Used when loading from file."""
    @staticmethod
    def char_to_state(c):
        if c == '.': return State.conductor
        if c == 't': return State.tail
        if c == 'H': return State.head
        if c == 'b' or c == ' ': return None
        raise Exception('Unknown char: ' + c)


"""The Wireworld engine stores the world current state
and provides a method to step to the next state."""
class Engine:

    def __init__(self, world):
        self.world = world
        self.clock = 0

        # All neighbors as delta (-1,-1 to 1, 1)
        self.dd = list(product([-1, 0, 1], repeat= 2))
        self.dd.remove((0, 0))

        # Compute dimensions
        values = world.values()
        points = list(itertools.chain(*values))
        self.size_x = max(points, key=lambda x: x[0])[0] + 1
        self.size_y = max(points, key=lambda x: x[1])[1] + 1


    """Return the current state of the world as a map.
    keys: State; values: list of (x, y) tuples.

    For instance:
        {
            State.head:      [(2, 0)],
            State.tail:      [(1, 0)],
            State.conductor: [(0, 0), (3, 0)]
        }
    """
    def state(self):
        return self.world


    def dimensions(self):
        return (self.size_x, self.size_y)

    def get_clock(self):
        return self.clock


    """Step the world to the next state."""
    def tick(self):
        new_world = Engine.__new_world()

        # conductor -> head if 1 or 2 neighbors, conductor otherwise
        for x, y in self.world[State.conductor]:
            n = sum((x+dx, y+dy) in self.world[State.head] for dx, dy in self.dd)
            if n == 1 or n == 2:
                state = State.head
            else:
                state = State.conductor
            new_world[state].append((x, y))

        new_world[State.tail] = self.world[State.head]       # head -> tail
        new_world[State.conductor] += self.world[State.tail]    # tail -> conductor

        self.world = new_world
        self.clock += 1
        return self.world



    """Create a new, empty world."""
    @staticmethod
    def __new_world():
        world = dict()
        for state in State:
            world[state] = []
        return world


    """Load a world from a file.
    Expected format: 1 char per cell.
    `.` = conductor, `H` = head, `t` = tail.
    A space `' '` or nothing at the end of the line: empty
    """
    @staticmethod
    def load(file_name):
        world = Engine.__new_world()

        with open(file_name, 'r') as f:
            for line_number, line in enumerate(f):
                line = line.rstrip('\n\r ')
                for char_index, c in enumerate(line):
                    state = State.char_to_state(c)
                    if state != None:
                        world[state].append((char_index, line_number))

        print(world)
        return world
