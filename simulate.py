#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" run a wireworld simulation on input file """

from engine import Engine

import argparse

import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib.patches import Rectangle

def get_blocks(state):
    result = []
    for state, points in state().items():
        for point in points:
            block = Rectangle(point, 1, 1,
                              edgecolor = 'black',
                              facecolor = state.value,
                              fill = True,
                              lw = 0.05)
            result.append(block)
    return result
    
def create_animation(world, n_frames, delay, width_inches=15):

    w_width, w_height = world.dimensions()

    fig = plt.figure()
    fig.set_size_inches([width_inches*f for f in [1, w_height/w_width]])
    plt.axis('off')

    ax = fig.add_subplot(1, 1, 1)
    ax.set_xlim(0, w_width)
    ax.set_ylim(0, w_height)

    frames = []
    for i in range(n_frames):
        label = ax.text(1, 0.92, str(world.get_clock()), fontsize=14,
                        horizontalalignment="right",
                        transform=ax.transAxes)
        artists = [label]
        for block in get_blocks(world.state):
            artists.append(ax.add_patch(block))

        frames.append(artists)
        world.tick()
        
    anim = animation.ArtistAnimation(fig, frames, interval=delay, blit=True)

    anim.save('images/adder.gif', dpi=80, writer='imagemagick')

    plt.show()


def main(path, delay=500, frames=49):
    initial_state = Engine.load(path)
    ww = Engine(initial_state)
    create_animation(ww, frames, delay)
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser('Wireworld simulation')
    parser.add_argument('world', help='File path to a wireworld')
    parser.add_argument('delay', help='Delay (ms) between each frame')
    args = parser.parse_args()
    main(args.world, args.delay)
