# Wireworld Adder

implement a binary adder in [Wireworld](https://en.wikipedia.org/wiki/Wireworld) in Python

![binary adder](images/adder.gif)

based on [Wireworld](https://github.com/yackx/wireworld) by [Youri Ackx](https://github.com/yackx).

## Wireworld

Cellular automaton simulating electronic ciruits

### States

0. empty
1. electron head
2. electron teil
3. conductor

### Rules

$`N`$ denotes number of electron heads in neighbouring cells ([Moore
neighbourhood](https://en.wikipedia.org/wiki/Moore_neighborhood),
i. e. all adjacent cells, orthogonally and diagonally are regarded as neighbours).

| current state | next state    | condition           |
| ------------- | ------------- | -----------------   |
| empty         | empty         |                     |
| electron head | electron tail |                     |
| electron tail | conductor     |                     |
| conductor     | electron head | if $`N=1 \lor N=2`$ |
| conductor     | conductor     | otherwise           |

#### Transition matrix

```math
\mathbf{T} = 
\begin{pmatrix}
	         \text{false} & \text{true} & \text{false} \\
		     \text{false} & \text{false} & \text{true} \\
		     N \in \{1, 2\} & \text{false} & N \notin \{1, 2\} \\
\end{pmatrix}			 
```

# References:

Mark Owen: [The Wireworld Computer](https://www.quinapalus.com/wi-index.html)

Vladislav Gladkikh, Alexandr Nigay: [Wirewolrd++: A Celluar Automaton
for Simulation of Nonplanar Digital Electronic
Circuits](http://wpmedia.wolfram.com/uploads/sites/13/2018/07/27-1-2.pdf)
